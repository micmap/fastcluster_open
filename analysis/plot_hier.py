import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker

plt.rcParams.update({'font.size': 9})

dire="../rapid_A1_NSC_chi01_output_noclusterevolv/Dyn/0.02/"

####################################################################
#LEGEND for 1st and higher generations
#c0:identifier
#c1:M1/Msun   primary mass
#c2:M2/Msun   secondary mass
#c3:chi1      spin magnitude of primary component
#c4:chi2      spin magnitude of secondary component
#c5:theta1    inclination between orbital angular momentum and spin vector of primary component
#c6:theta2    inclination between orbital angular momentum and spin vector of primary component
#c7:SMA(Rsun) initial semi-major axis
#c8:ecc       initial eccentricity
#c9:tDF+min(t12,t3bb)/Myr    formation time (for higher generations DOES NOT INCLUDE previous generations)
#c10:SMAfin(cm)    semi-major axis after Peters (1964) integration (just for check)
#c11:eccfin        eccentricity after Peters (1964) integration (just for check)
#c12:tpeters/Myr   time of Peters (1964) integration
#c13:(tDF+t3bb+tpeters)/Myr time of merger (time of formation + time of Peters (1964) integration) NOTE: for higher generations c13 includes the time for previous generations
#c14:vkick/kms     relativistic recoil kick
#c15:mrem/Msun     mass of merger remnant (after GR correction)
#c16:arem          spin of merger remnant (after GR correction)
#c17:vesc/kms      escape velocity from the cluster
#c18:flag1         internal flag
#c19:flag2         ONLY internal flag important for you: if flag2=="merg" the binary is a merger
#c20:flag3         internal flag
#c21:flagSN        internal flag for SN explosion
#c22:flag_exch     internal flag to tell you if binary was ejected
#c23:flag_t3bb     internal flag to check if the binary system can form or not
#c24:flag_evap     internal flag to check if the star cluster has evaporated
#c25:Mtot/Msun     total star cluster mass
#c26:ecc(10Hz)     binary eccentricity at 10 Hz
#c27:Ngen          number of generations
###############################à


#############READ FIRST GENERATION ######################
first_m1,first_m2,first_a1,first_a2,first_th1,first_th2,first_sma,first_ecc,first_t3bb,first_tmerg,first_kick,first_mmerg,first_amerg, first_vesc,first_ecc10=np.genfromtxt(dire+"first_gen.txt", usecols=(1,2,3,4,5,6,7,8,9,13,14,15,16,17,26),unpack=True,skip_header=1)
first_flag2,first_flag3,first_flagSN,first_flag_exch,first_flag_t3bb,fist_flag_evap=np.genfromtxt(dire+"first_gen.txt", usecols=(19,20,21,22,23,24),dtype=str,unpack=True,skip_header=1)


aa=np.where((first_flag2=="merg")) 
v=aa[0]
print(len(v))

############ READ SECOND GENERATION MERGERS ################
idi,m1,m2,a1,a2,th1,th2,sma,t3bb,ecc,tmerg,kick,mmerg,amerg,vesc,Mtot,ecc10 =np.genfromtxt(dire+"nth_generation.txt", usecols=(0,1,2,3,4,5,6,7,9,11,13,14,15,16,17,25,26),unpack=True,skip_header=1)
flag2,flag3,flagSN,flag_exch,flag_t3bb,flag_evap=np.genfromtxt(dire+"nth_generation.txt", usecols=(19,20,21,22,23,24),dtype=str,unpack=True,skip_header=1)

bb=np.where((flag2=="merg"))
vv=bb[0]
print(len(vv))



############# PLOT PRIMARY AND SECONDARY MASS ###############
fig,ax=plt.subplots(3,2)
binno=np.logspace(np.log10(min(first_m2)),np.log10(max(m1)+10.), num=30)
ax[0][0].hist(first_m1[v],histtype="stepfilled", density=False, color="red", bins=binno, alpha=0.3, log=False,zorder=2)
ax[0][0].hist(first_m2[v],histtype="stepfilled", density=False, color="blue", bins=binno, alpha=0.3, log=False,zorder=1)

ax[0][0].hist(m1[vv],histtype="step", density=False, color="red", bins=binno, log=False,zorder=4)
ax[0][0].hist(m2[vv],histtype="step", density=False, color="blue", bins=binno, log=False,zorder=3)

print("m1max= ",max(m1))
print("m2max= ",max(m2))
ax[0][0].legend(["$m_{1}$","$m_{2}$","$m_{1,\,{}\mathrm{ng}}$","$m_{2,\,{}\mathrm{ng}}$"],ncol=2,loc="lower left", fontsize=7)

ax[0][0].set_yscale("log")
ax[0][0].set_xscale("log")
ax[0][0].set_xlabel("Mass [M$_\odot$]")
ax[0][0].set_ylabel("$N_\mathrm{sim}$")


################ PLOT PRIMARY AND SECONDARY SPIN PARAMETERS (chieff, chip) #################
binno=np.linspace(0.0,1.0, num=30)
ax[0][1].hist(first_a1[v],histtype="stepfilled", density=False, color="red", bins=binno,alpha=0.3)
ax[0][1].hist(first_a2[v],histtype="stepfilled", density=False, color="blue", bins=binno,alpha=0.3)
print(min(a1[vv]),max(a1[vv]))
ax[0][1].hist(a1[vv],histtype="step", density=False, color="red", bins=binno)
ax[0][1].hist(a2[vv],histtype="step", density=False, color="blue", bins=binno)


ax[0][1].legend(["$\chi_{1}$","$\chi_{2}$","$\chi_{1,\,{}\mathrm{ng}}$","$\chi_{2,\,{}\mathrm{ng}}$"],ncol=2,loc="upper left", fontsize=7)
ax[0][1].set_xlabel("Spin Magnitude")
ax[0][1].set_yscale("log")

x_major = [0.0,0.2,0.4,0.6,0.8,1.0]
ax[0][1].xaxis.set_major_locator(ticker.FixedLocator(x_major))

x_minor = ticker.MultipleLocator(0.05)
ax[0][1].xaxis.set_minor_locator(x_minor)
ax[0][1].xaxis.set_minor_formatter(ticker.NullFormatter())

############ CALCULATES CHIEFF ######################
first_chieff=(first_m1*first_a1*np.cos(first_th1)+first_m2*first_a2*np.cos(first_th2))/(first_m1+first_m2)

chieff=(m1*a1*np.cos(th1)+m2*a2*np.cos(th2))/(m1+m2)

fq=first_m2/first_m1


############### CALCULATES CHIP ###################
first_chip=1./((2.+3.*0.5*fq)*first_m1**2.) #2.99792458e+10/((2.+3.*0.5*fq)*6.6743e-08*first_m1**2.)

for i in range(len(first_m1)):
    first_chip[i]=first_chip[i] * max((2.+3.*0.5*fq[i])*first_a1[i]*first_m1[i]**2 *np.sin(first_th1[i]),(2.+3.*0.5/fq[i])*first_a2[i]*first_m2[i]**2 * np.sin(first_th2[i]))

fq=m2/m1

chip=1./((2.+3.*0.5*fq)*m1**2.) #2.99792458e+10/((2.+3.*0.5*fq)*6.6743e-08*m1**2.)
for i in range(len(m1)):
    chip[i]=chip[i] * max((2.+3.*0.5*fq[i])*a1[i]*m1[i]**2 *np.sin(th1[i]),(2.+3.*0.5/fq[i])*a2[i]*m2[i]**2 * np.sin(th2[i]))

binno=np.linspace(-1.0,1.0, num=50)
ax[1][0].hist(first_chieff[v],histtype="stepfilled", density=False, color="red", bins=binno,alpha=0.3)
ax[1][0].hist(chieff[vv],histtype="step", color="red",density=False, bins=binno)
ax[1][0].hist(first_chip[v],histtype="stepfilled", density=False, color="blue", bins=binno,alpha=0.3)
ax[1][0].hist(chip[vv],histtype="step", color="blue",density=False, bins=binno)
ax[1][0].legend(["$\chi_\mathrm{eff}$","$\chi_\mathrm{eff,\,{}ng}$","$\chi_\mathrm{p}$","$\chi_\mathrm{p,\,{}ng}$"],ncol=1,loc="upper left", fontsize=7)
ax[1][0].set_yscale("log")
ax[1][0].set_xlabel("$\chi_\mathrm{eff}$, $\chi_\mathrm{p}$")
ax[1][0].set_ylabel("$N_\mathrm{sim}$")
ax[1][0].set_xlim(-1.0,1.0)

x_major = [-0.9,-0.6,-0.3,0,0.3,0.6,0.9]
ax[1][0].xaxis.set_major_locator(ticker.FixedLocator(x_major))

x_minor = ticker.MultipleLocator(0.1)
ax[1][0].xaxis.set_minor_locator(x_minor)
ax[1][0].xaxis.set_minor_formatter(ticker.NullFormatter())


####### PLOTS ECCENTRICITY AT 10 Hz ##################
binss=np.logspace(np.log10(min(first_ecc10[v])),np.log10(1.0),num=30)

ax[1][1].hist(first_ecc10[v],histtype="stepfilled", density=False, bins=binss, alpha=0.3)
ax[1][1].hist(ecc10[vv],histtype="step", density=False, bins=binss)
ax[1][1].legend(["$e$","$e_\mathrm{ng}$"],ncol=1,loc="upper left", fontsize=7)
ax[1][1].set_xlabel("Eccentricity at $f_\mathrm{GW}=10\,{}\mathrm{Hz}$")
ax[1][1].set_yscale("log")
ax[1][1].set_xscale("log")

x_major = [1e-8,1e-6,1e-4,1e-2,1]
ax[1][1].xaxis.set_major_locator(ticker.FixedLocator(x_major))

x_minor = ticker.LogLocator(base = 10.0, subs = np.arange(1.0, 10.0) * 0.1, numticks = 5)
ax[1][1].xaxis.set_minor_locator(x_minor)
ax[1][1].xaxis.set_minor_formatter(ticker.NullFormatter())


######### PLOTS FORMATION AND MERGER TIMES #################
binss=np.logspace(np.log10(min(min(first_t3bb[v]),min(t3bb[vv]))),4.176091259055681,num=30)

ax[2][0].hist(first_t3bb[v],histtype="stepfilled", density=False, bins=binss,color="black",alpha=0.3)
ax[2][0].hist(first_tmerg[v],histtype="stepfilled", density=False, bins=binss,color="green",alpha=0.3)
ax[2][0].hist(t3bb[vv],histtype="step", density=False, bins=binss,color="black")
ax[2][0].hist(tmerg[vv],histtype="step", density=False, bins=binss,color="green")

ax[2][0].legend(["$t_\mathrm{dyn}$","$t_\mathrm{merg}$", "$t_\mathrm{dyn,\,{}ng}$","$t_\mathrm{merg,\,{}ng}$"],ncol=1,loc="upper left", fontsize=7)
#ax[2][0].set_xlim(1e-4,20.)
ax[2][0].set_yscale("log")
ax[2][0].set_xscale("log")
#ax[2][0].legend(["T$_{3bb}$", "T$_{merg}$"])
ax[2][0].set_xlabel("Timescales [Myr]")
ax[2][0].set_ylabel("$N_\mathrm{sim}$")

x_major = [0.1,1,1e1,1e2,1e3,1e4]
ax[2][0].xaxis.set_major_locator(ticker.FixedLocator(x_major))

x_minor = ticker.LogLocator(base = 10.0, subs = np.arange(1.0, 10.0) * 0.1, numticks = 10)
ax[2][0].xaxis.set_minor_locator(x_minor)
ax[2][0].xaxis.set_minor_formatter(ticker.NullFormatter())


######## PLOTS KICK AND ESCAPE VELOCITY #######################

binss=np.logspace(0.,np.log10(max(kick[vv]+10.)),num=30)

ax[2][1].hist(first_kick[v],histtype="stepfilled", density=False, bins=binss,alpha=0.3)
ax[2][1].hist(kick[vv],histtype="step", density=False, bins=binss)


ax[2][1].hist(first_vesc[v],histtype="stepfilled", density=False, bins=binss,alpha=0.3,color="green")
ax[2][1].hist(vesc[vv],histtype="step", density=False, bins=binss, color="green")

ax[2][1].set_xlabel("Kick and escape velocity [km s$^{-1}$]")
ax[2][1].legend(["$V_\mathrm{k}$","$V_\mathrm{k,\,{}ng}$","$V_\mathrm{esc}$","$V_\mathrm{esc,\,{}ng}$"],ncol=1,loc="upper left", fontsize=7)
#ax[2][1].set_yscale("log")
ax[2][1].set_xscale("log")
ax[2][1].set_yscale("log")
#ax[2][1].set_ylim([0.1,1e6])

plt.tight_layout()
plt.savefig(dire+"dyn_BBHmergers.png")
plt.savefig(dire+"dyn_BBHmergers.pdf")
plt.show()
